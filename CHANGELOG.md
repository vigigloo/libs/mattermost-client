# Changelog

### [v0.1.2](https://gitlab.com/vigigloo/libs/mattermost-client/compare/v0.1.1...v0.1.2) (2023-05-13)

#### Fixes

- **openapi/definition:** PostMetadata
  ([1b1a9c9](https://gitlab.com/vigigloo/libs/mattermost-client/commit/1b1a9c9c8da798cd350496262595d4c4cec839aa))

### [v0.1.1](https://gitlab.com/vigigloo/libs/mattermost-client/compare/v0.1.0...v0.1.1) (2023-05-12)

#### Fixes

- **client:** Sanitize base URL
  ([a0563f5](https://gitlab.com/vigigloo/libs/mattermost-client/commit/a0563f58edd7d41d2686c1f6a4f8de6986b7c1e5))

## v0.1.0 (2023-05-12)

- Initial release
