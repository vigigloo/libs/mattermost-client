use crate::openapi::{
    apis::{posts_api as apis, Result},
    models::{self, CreatePostRequestMetadata},
};
use typed_builder::TypedBuilder;

use crate::Client;

#[derive(Debug, TypedBuilder)]
#[builder(doc, field_defaults(default, setter(strip_option)))]
pub struct CreatePost {
    /// The channel ID to post in
    #[builder(!default, setter(into, !strip_option))]
    pub channel_id: String,
    /// The message contents, can be formatted with Markdown
    #[builder(!default, setter(into, !strip_option))]
    pub message: String,
    /// The post ID to comment on
    pub root_id: Option<String>,
    /// A list of file IDs to associate with the post. Note that posts are limited to 5 files maximum. Please use additional posts for more files.
    pub file_ids: Option<Vec<String>>,
    /// A general JSON property bag to attach to the post
    pub props: Option<serde_json::Value>,
    pub metadata: Option<CreatePostRequestMetadata>,
    /// Whether to set the user status as online or not
    pub set_online: Option<bool>,
}

impl CreatePost {
    pub async fn send(self, client: &Client) -> Result<models::Post, apis::CreatePostError> {
        let params = apis::CreatePostParams {
            create_post_request: models::CreatePostRequest {
                channel_id: self.channel_id,
                message: self.message,
                root_id: self.root_id,
                file_ids: self.file_ids,
                props: self.props,
                metadata: self.metadata.map(Box::new),
            },
            set_online: self.set_online,
        };

        apis::create_post(client.configuration(), params).await
    }
}
