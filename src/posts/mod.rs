use crate::Client;

pub mod create;

#[derive(Debug, Clone, Copy)]
pub struct Posts;

impl Posts {
    pub fn create_post(self) -> create::CreatePostBuilder {
        create::CreatePost::builder()
    }
}

impl Client {
    /// Access the Mattermost Posts API
    pub fn posts(&self) -> Posts {
        Posts
    }
}
