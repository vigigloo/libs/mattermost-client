use crate::openapi::apis::configuration::Configuration;

const DEFAULT_USER_AGENT: &str = concat!("mattermost-client/", env!("CARGO_PKG_VERSION"));

/// Mattermost API library entry point.
///
/// This type serve as an entrypoint for the Mattermost API through
/// different "bridges" for the differents API endpoints categories
/// (e.g. [Client::posts])
pub struct Client {
    configuration: Configuration,
}

impl Client {
    pub fn new(base_url: impl Into<String>) -> Self {
        let base_url: String = base_url.into();
        let base_url = base_url.trim_end_matches("/");

        Self {
            configuration: Configuration {
                base_path: base_url.to_owned(),
                user_agent: Some(DEFAULT_USER_AGENT.into()),
                ..Default::default()
            },
        }
    }

    pub fn with_user_agent(mut self, user_agent: impl Into<String>) -> Self {
        self.set_user_agent(user_agent);
        self
    }

    pub fn set_user_agent(&mut self, user_agent: impl Into<String>) -> &mut Self {
        self.configuration.user_agent = Some(user_agent.into());
        self
    }

    pub fn with_access_token(mut self, access_token: impl Into<String>) -> Self {
        self.set_access_token(access_token);
        self
    }

    pub fn set_access_token(&mut self, access_token: impl Into<String>) -> &mut Self {
        self.configuration.bearer_access_token = Some(access_token.into());
        self
    }

    pub fn configuration(&self) -> &Configuration {
        &self.configuration
    }

    pub fn configuration_mut(&mut self) -> &mut Configuration {
        &mut self.configuration
    }
}
