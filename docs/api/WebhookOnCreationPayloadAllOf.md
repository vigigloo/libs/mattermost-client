# WebhookOnCreationPayloadAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channel_url** | Option<**String**> | Absolute URL to the playbook run's channel. | [optional]
**details_url** | Option<**String**> | Absolute URL to the playbook run's details. | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


