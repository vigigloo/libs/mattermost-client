# `mattermost-client`

This crate is an attempt to provde a sane Rust interface to the [Mattermost API](docs/README.md).

## Example

```rust
use mattermost_client::Client;

let client = Client::new("https://<YOUR MATTERMOST INSTANCE URL>")
    .with_user_agent("MyLittleSoftware/1.0")
    .with_access_token("<YOUR MATTERMOST ACCESS TOKEN>");

client
    .posts()
    .create_post()
    .channel_id("#welcome")
    .props(serde_json::json!({
        "name": "Example",
    }))
    .message("Hello world!")
    .build()
    .send(&client)
    .await
    .into_diagnostic()?;
```

## Installation

Add this in your `Cargo.toml`:

```toml
[dependencies]
mattermost-client = "0.1"
```
